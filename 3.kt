import java.lang.StringBuilder

fun main() {
    val string = readLine()
    if (string == null) {
        println("Empty input")
        return
    }

    val dp = Array(string.length) { IntArray(string.length) }
    val previous = Array(string.length) { Array<BorderMove?>(string.length) { null } }
    for (i in 0..string.lastIndex) {
        dp[i][i] = 1
    }

    for (length in 2..string.length) {
        for (left in 0..string.length - length) {
            val right = left + length - 1
            if (length == 2 && string[right] == string[left]) {
                dp[left][right] = 2
            } else if (string[right] == string[left]) {
                dp[left][right] = dp[left + 1][right - 1] + 2
                previous[left][right] = BorderMove.BOTH
            } else {
                if (dp[left + 1][right] > dp[left][right - 1]) {
                    dp[left][right] = dp[left + 1][right]
                    previous[left][right] = BorderMove.LEFT
                } else {
                    dp[left][right] = dp[left][right - 1]
                    previous[left][right] = BorderMove.RIGHT
                }
            }
        }
    }

    val border = Border(0, string.lastIndex)
    val answerBuilder = StringBuilder()
    var direction = previous[border.left][border.right]
    while (direction != null) {
        if (direction == BorderMove.BOTH) {
            answerBuilder.append(string[border.right])
        }
        border.apply(direction)
        direction = previous[border.left][border.right]
    }

    val answer = if (border.length == 2) {
        answerBuilder.append(string[border.right])
        answerBuilder.toString() + answerBuilder.toString().reversed()
    } else {
        answerBuilder.toString() + string[border.right] + answerBuilder.toString().reversed()
    }
    println(answer)
}

enum class BorderMove {
    RIGHT, LEFT, BOTH
}

data class Border(var left: Int, var right: Int) {
    val length
        get() = right - left + 1

    fun apply(borderMove: BorderMove) {
        when (borderMove) {
            BorderMove.RIGHT -> right--
            BorderMove.LEFT -> left++
            BorderMove.BOTH -> {
                right--
                left++
            }
        }
    }
}
